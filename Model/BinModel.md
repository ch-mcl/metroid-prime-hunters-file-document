# Structure

Header

# Header
section size is 98 Byte.

| Offset | Size (Byte) | Type | Description |
| ------ | ----------- | ---- | ----------- |
| 0x00  | 4 | u32 | scale1 (node will be absolute size) |
| 0x04 | 4 | u32 | scale2 (node will be relative size) |
| 0x08 | 4 | u32 | Unk_0x08 |
| 0x0C | 4 | u32 | Unk_0x0C |
| 0x10 | 4 | u32 | [Material](#material) Offset |
| 0x14 | 4 | u32 | [Dlist](#dlist) Offset |
| 0x18 | 4 | u32 | [Node](#node) Offset |
| 0x1C | 4 | u32 | Unk_0x1C |
| 0x20 | 4 | u32 | (size of header?) |
| 0x24 | 4 | u32 | [Mesh](#mesh) Offset |
| 0x28 | 4 | u32 | Count of Texture |
| 0x2C | 4 | u32 | [Texture](#texture) Offset |
| 0x30 | 4 | u32 | Count of Pallet |
| 0x34 | 4 | u32 | [Pallet](#pallet) Offset |
| 0x38 | 4 | u32 | NodeWeight  Offset | 
| 0x3C | 4 | u32 | Unk_0x3C |
| 0x40 | 4 | u32 | Unk_0x40 |
| 0x44 | 4 | u32 | Unk_0x44 |
| 0x48 | 2 | u16 | Count of Material |
| 0x4A | 2 | u16 | Count of Node |
| 0x4E | 4 | u32 | Unk_0x4E |
| 0x52 | 4 | u32 | Unk_0x52 |
| 0x56 | 4 | u32 | Unk_0x56 |
| 0x5A | 4 | u32 | Unk_0x5A |
| 0x5E | 4 | u32 | Unk_0x5E |
| 0x60 | 2 | u16 | Count of Mesh |
| 0x62 | 2 | u16 | Count of something |



# Material
Section size is 132 Byte.

| Offset | Size (Byte) | Type | Description |
| ------ | ----------- | -----| ----------- |
| 0x00 | 64 | char[64] | Material Name. |
| 0x40 | 1 | u8 | Light Setting. See [Light Seeting](#light-setting).|
| 0x41 | 1 | u8 | Culling Setting. See [Culling Setting](#culling-setting) |
| 0x42 | 2 | u16 | Opacity/Transparent. |
| 0x44 | 2 | u16 | Pallet ID |
| 0x46 | 2 | u16 | Texture ID |
| 0x48 | 1 | u8 | Wrap Setting Y. See [Wrap Setting](#wrap-setting).|
| 0x49 | 1 | u8 | Wrap Setting X. See [Wrap Setting](#wrap-setting).|
| 0x4A | 3 | u8[3] | Ambient Color |
| 0x4D | 3 | u8[3] | Diffuse Color |
| 0x50 | 3 | u8[3] | Specular Color |
| 0x53 | 1 | u8 | Unk_0x53 |
| 0x54 | 1 | u8 | alpha part setting. See [Alpha Part Setting](#alpha-part-setting). |
| 0x58 | 4 | - | Unk_0x58 |
| 0x5C | 4 | - | Unk_0x5C |
| 0x60 | 4 | - | Unk_0x60 |
| 0x64 | 4 | - | Unk_0x64 |
| 0x68 | 4 | - | Unk_0x68 |
| 0x6C | 4 | - | Unk_0x6C |
| 0x70 | 4 | - | Unk_0x70 |
| 0x74 | 4 | - | Unk_0x74 |
| 0x78 | 4 | - | Unk_0x78 |
| 0x7C | 4 | - | Unk_0x7C |
| 0x80 | 4 | - | Unk_0x80 |


## Light Setting

| Value | Description |
| ----- | ----------- |
| 0 | FALSE |
| 1 | TRUE |

Perhaps this is boolean.

## Culling Setting

| Value | Description |
| ----- | ----------- |
| 0 | Double-sided |
| 1 | back |
| 2 | front |

## Wrap Setting

| Value | Description |
| ----- | ----------- |
| 0 | clamp |
| 1 | repeat |
| 2 | mirror |

## Alpha Part Setting

| Value | Description |
| ----- | ----------- |
| 0 | none |
| 1 | disable alpha |
| 2 | alpha black |
| 3 | invisible |

# DList
Section size is 32 Byte.

| Offset | Size (Byte) | Type | Description |
| ------ | ----------- | -----| ----------- |
| 0x00 | 4 | u32 | [Data](#data) Offset |
| 0x04 | 4 | u32 | Size |
| 0x08 | 4 | u32 | X Minimum |
| 0x0C | 4 | u32 | Y Minimum |
| 0x10 | 4 | u32 | Z Minimum |
| 0x14 | 4 | u32 | X Maximum |
| 0x18 | 4 | u32 | Y Maximum |
| 0x1C | 4 | u32 | Z Maximum |

## Data
Structure is like this.

| Offset | Size (Byte) | Type | Description |
| ------ | ----------- | -----| ----------- |
| 0x00 | 4 | u8 | Command (for Data_1) |
| 0x04 | 4 | u8 | Command (for Data_2) |
| 0x08 | 4 | u8 | Command (for Data_3) |
| 0x0C | 4 | u8 | Command (for Data_4) |
| 0x10 | - | - | Data_1 |
| 0x10 + size of Data_1 | - | - | Data_2 |
| Offset of Data_1 + size of Data_2 | - | - | Data_3 |
| Offset of Data_2 + size of Data_3 | - | - | Data_4 |

### Command

| Type | Description | Data Description |
| -----| ----------- | ---------------- |
| 0x14 | Node number | relation to Node? |
| 0x20 | color(rgb555) | r:5bit g:5bit b:5bit |
| 0x21 | normal | x:10bit y:10bit z:10bit ?:2bit |
| 0x22 | UV | U:2byte, V:2byte |
| 0x23 | 16bit Vertex | x:2byte y:2byte z:2byte ?:2byte |
| 0x24 | 10bit Vertex | x:10bit y:10bit z:10bit ?:2bit |
| 0x25 | different of XY | X:2byte, Y:2byte |
| 0x26 | different of XZ | X:2byte, Z:2byte |
| 0x27 | different of YZ | Y:2byte, Z:2byte |
| 0x28 | 10bit different of XYZ | x:10bit y:10bit z:10bit ?:2bit |
| 0x40 | mode | polygon See [mode](#mode). |

### mode
these are looks same to "gleBegin" function's GLenum.

| Type | Description (Written by GLenum) |
| -----| ----------- |
| 0x00 | GL_TRIANGLES |
| 0x01 | GL_QUADS |
| 0x02 | GL_TRIANGLE_STRIP |
| 0x03 | GL_QUAD_STRIP |
| 0x04 | GL_TRIANGLE_FAN |


# Node
Section size is 240 Byte.

| Offset | Size (Byte) | Type | Description |
| ------ | ----------- | -----| ----------- |
| 0x00 | 64 | char[64] | Name of Node |
| 0x40 | 2 | u16 | Parent Node number (0xFFFF mean Parent?) |
| 0x42 | 2 | u16 | Child Node number (0xFFFF mean none Child?) |
| 0x44 | 2 | u16 | Unk_0x44 |
| 0x46 | 2 | u16 | Unk_0x46 |
| 0x48 | 4 | u32 | Unk_0x48 |
| 0x4C | 4 | u32 | Unk_0x4C |
| 0x50 | 4 | u32 | X size (default: 0x1000) |
| 0x54 | 4 | u32 | Y size (default: 0x1000) |
| 0x58 | 4 | u32 | Z size (default: 0x1000) |
| 0x5C | 4 | u32 | Unk_0x5C |
| 0x60 | 4 | u32 | Unk_0x60 |
| 0x64 | 4 | u32 | Unk_0x64 |
| 0x68 | 4 | u32 | Unk_0x68 |
| 0x6C | 4 | u32 | Unk_0x6C |
| 0x70 | 4 | u32 | Starting draw distance ?  |
| 0x74 | 4 | u32 | X Minimum |
| 0x78 | 4 | u32 | Y Minimum |
| 0x7C | 4 | u32 | Z Minimum |
| 0x80 | 4 | u32 | X Maximum |
| 0x84 | 4 | u32 | Y Maximum |
| 0x88 | 4 | u32 | Z Maximum |
| 0x8C | 4 | u32 | Unk_0x8C |
| 0x90 | 4 | u32 | Unk_0x90 |
| 0x94 | 4 | u32 | Unk_0x94 |
| 0x9C | 4 | u32 | Unk_0x9C |
| 0xA0 | 4 | u32 | Unk_0xA0 |
| 0xA4 | 4 | u32 | Unk_0xA4 |
| 0xA8 | 4 | u32 | Unk_0xA8 |
| 0xAC | 4 | u32 | Unk_0xAC |
| 0xB0 | 4 | u32 | Unk_0xB0 |
| 0xB4 | 4 | u32 | Unk_0xB4 |
| 0xB8 | 4 | u32 | Unk_0xB8 |
| 0xBC | 4 | u32 | Unk_0xBC |
| 0xC0 | 4 | u32 | Unk_0xC0 |
| 0xC4 | 4 | u32 | Unk_0xC4 |
| 0xC8 | 4 | u32 | Unk_0xC8 |
| 0xCC | 4 | u32 | Unk_0xCC |
| 0xD0 | 4 | u32 | Unk_0xD0 |
| 0xD4 | 4 | u32 | Unk_0xD4 |
| 0xD8 | 4 | u32 | Unk_0xD8 |
| 0xDC | 4 | u32 | Unk_0xDC |
| 0xE0 | 4 | u32 | Unk_0xE0 |
| 0xE4 | 4 | u32 | Unk_0xE4 |
| 0xE8 | 4 | u32 | Unk_0xE8 |
| 0xEC | 4 | u32 | Unk_0xEC |


# Mesh
Section size is 4 Byte.

| Offset | Size (Byte) | Type | Description |
| ------ | ----------- | -----| ----------- |
| 0x00 | 2 | u16 | [Material](#material) ID |
| 0x02 | 2 | u16 | [Dlist](#dlist) ID |

# Texture
Section size is 40 Byte.

| Offset | Size (Byte) | Type | Description |
| ------ | ----------- | -----| ----------- |
| 0x00 | 2 | u16 | Texture Format. see [Texture Format](#texture-format) |
| 0x02 | 2 | u16 | Width |
| 0x04 | 2 | u16 | Height |
| 0x06 |2 | - | Padding |
| 0x08 | 4 | u32 | Texture Data offset |
| 0x0C | 4 | u32 | Texture Data size |
| 0x10 | 4 | ? | Unk_0x10 |
| 0x14 | 4 | ? | Unk_0x14 |
| 0x18 | 4 | ? | Unk_0x18 |
| 0x1C | 4 | ? | Unk_0x1C |
| 0x20 | 4 | ? | Unk_0x20 |
| 0x24 | 4 | ? | Unk_0x24 |

## Texture Format
| Format | Bpp | Pallet Size (Byte) | Alpha (Bit) |
| ------ | --- | ------------------ | ----------- |
| 0 | 2 | 16 | NONE |
| 1 | 4 | 32 | NONE |
| 2 | 8 | 32 | NONE |
| 3 | - | - | - |
| 4 | 8 | 16 | 5 |
| 5 | 16 | 256 | NONE |
| 6 | 8 | 32 | 3 |

Format 3 Couldn’t find on Metroid Prime: Hunters.

# Pallet
Section size is 16 Byte.

| Offset | Size (Byte) | Type | Description |
| ------ | ----------- | ---- | ----------- |
| 0x00 | 4 | u32 | Pallet Data Offset |
| 0x04 | 4 | u32 | Pallet Data Size |
| 0x08 | 4 | - | Unk_0x08 |
| 0x0C | 4 | - | Unk_0x0C |


# Weight
